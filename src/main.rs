use std::cell::RefMut;

use color_eyre::eyre::{eyre, Result};
use display_cli::{
    commit_changes, query_displays, DisplaySettings, FixedOutput, Orientation, Position, Resolution,
};
use structopt::{clap::ArgGroup, StructOpt};

// Cli arguments
#[derive(StructOpt, Debug)]
#[structopt(
    name = "display-cli",
    about = "Allows changing display settings on Windows using the CLI."
)]
struct Opts {
    #[structopt(subcommand)]
    cmd: SubCommands,
    /// Output debug info
    #[structopt(short, long, global = true)]
    verbose: bool,
}

#[derive(StructOpt, Debug)]
enum SubCommands {
    #[structopt(help = "Sets the primary display", alias = "sp")]
    SetPrimary {
        #[structopt(short, long, help = "Set the primary display")]
        id: usize,
    },
    #[structopt(help = "Changes settings of the primary display", alias = "p")]
    Primary {
        #[structopt(flatten)]
        properties: PropertiesOpt,
    },
    #[structopt(
        help = "Changes settings of a display with a specified id",
        alias = "props"
    )]
    Properties {
        #[structopt(
            required_unless = "primary",
            short,
            long,
            help = "Set properties of the display with the given id"
        )]
        id: usize,
        #[structopt(flatten)]
        properties: PropertiesOpt,
    },
}

#[derive(StructOpt, Debug)]
#[structopt(group = ArgGroup::with_name("prop").required(true).multiple(true))]
struct PropertiesOpt {
    #[structopt(
        group = "prop",
        short,
        long,
        help = "Set the position of the display",
        long_help = "Set the position of the display. Expected format: `<x>,<y>`"
    )]
    position: Option<Position>,
    #[structopt(
        group = "prop",
        short,
        long,
        help = "Sets the resolution of the display",
        long_help = "Sets the resolution of the display. Expected format: `<width>x<height>`."
    )]
    resolution: Option<Resolution>,
    #[structopt(
        group = "prop",
        short,
        long,
        help = "Sets the orientation of the display",
        long_help = "Sets the orientation of the display. One of: `Default`, `UpsideDown`, `Right`, `Left`"
    )]
    orientation: Option<Orientation>,
    #[structopt(
        group = "prop",
        short,
        long,
        help = "Sets the fixed output of the display",
        long_help = "Sets the fixed output of the display. One of: `Default`, `Stretch`, `Center`."
    )]
    fixed_output: Option<FixedOutput>,
}

/// Entry point for `display-cli`.
fn main() -> Result<()> {
    let _ = color_eyre::install()?;

    let opts = Opts::from_args();

    let log_level = if opts.verbose {
        log::LevelFilter::Trace
    } else {
        log::LevelFilter::Info
    };

    let _ = env_logger::Builder::from_env(
        env_logger::Env::default().default_filter_or(log_level.as_str()),
    )
    .init();

    log::debug!("Parsed Opts:\n{:#?}", opts);

    let display_set = query_displays()?;
    log::debug!("Discovered displays:\n{}", display_set);

    match opts.cmd {
        SubCommands::SetPrimary { id } => {
            let display = display_set
                .get(id)
                .ok_or_else(|| eyre!("Display with id {} not found", id))?;

            display.set_primary()?;

            display_set.apply()?;
            commit_changes()?;
            log::info!("Display settings changed");
        }
        SubCommands::Primary { properties } => {
            let display = display_set.primary();

            if let Some(settings) = display.settings() {
                let mut settings = settings.borrow_mut();
                set_properties(&properties, &mut settings);
            } else {
                Err(eyre!("Primary display has no settings"))?;
            }

            display.apply()?;
            commit_changes()?;
            log::info!("Display settings changed");
        }
        SubCommands::Properties { id, properties } => {
            let display = display_set
                .get(id)
                .ok_or_else(|| eyre!("Display with id {} not found", id))?;

            if let Some(settings) = display.settings() {
                let mut settings = settings.borrow_mut();
                set_properties(&properties, &mut settings)
            } else {
                Err(eyre!("Display has no settings"))?;
            }

            display.apply()?;
            commit_changes()?;
            log::info!("Display settings changed");
        }
    }

    Ok(())
}

macro_rules! assign_if_ok {
    ($properties:expr, $settings:expr, $name:ident) => {
        if let Some(value) = $properties.$name {
            $settings.$name = value;
        }
    };
}

fn set_properties(properties: &PropertiesOpt, settings: &mut RefMut<DisplaySettings>) {
    assign_if_ok!(properties, settings, position);
    assign_if_ok!(properties, settings, resolution);
    assign_if_ok!(properties, settings, orientation);
    assign_if_ok!(properties, settings, fixed_output);
}
