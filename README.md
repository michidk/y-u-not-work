# display CLI

[![MIT License](https://img.shields.io/crates/l/display_cli)](https://choosealicense.com/licenses/mit/) [![Continuous integration](https://github.com/michidk/display-cli/workflows/Continuous%20Integration/badge.svg)](https://github.com/michidk/display-cli/actions) [![Crates.io](https://img.shields.io/crates/v/display_cli)](https://crates.io/crates/display_cli)
[![Chocolatey](https://img.shields.io/chocolatey/v/display-cli?include_prereleases)](https://community.chocolatey.org/packages/display-cli)
